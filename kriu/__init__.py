# coding=utf8
"""
Kriu bot
"""

import sopel.module
import sopel.formatting
import requests
from bs4 import BeautifulSoup

DICT_URL = 'http://zodynas.kriu.lt/zodis/'
MESSAGE_LENGTH = 475

def get_page(word: str) -> str:
    """Retrieves html contents of kriu youth dictionary.
    
    Arguments:
        word {str} -- word to search for in dictionary
    
    Returns:
        str -- HTML contents
    """
    page = requests.get(DICT_URL + word)
    return page.text


def get_definitions(html_contents:str) -> list:
    """Parses out definitions from kriu youth dictionary
    
    Arguments:
        html_contents {str} -- contents of the page
    
    Returns:
        list -- list of definitions, elements should be of type string
    """
    definitions = []
    soup = BeautifulSoup(html_contents, 'html.parser')
    for div in soup.findAll('div', {'class': 'divZodis'}, recursive=True):
        if len(div.get('class')) == 1:
            definitions.append(div.find(class_='vertimas').text)

    return definitions

def format_response(word, definitions: list) -> str:
    """Formats an IRC response for definitions
    
    Arguments:
        definitions {list} -- list of definitions from Kriu youth dictionary
    
    Returns:
        str -- IRC formatted message: word: top 3 (if short enough) definitions -> link
    """
    if not definitions:
        return sopel.formatting.bold('Nesvaik, ') + sopel.formatting.underline('nėr tokio žodžio, netiki? ')

    response = sopel.formatting.bold(word) + ': '
    definitions = definitions[0:3] # Top 3
    for num, definition in enumerate(definitions):
        formatted_definition = sopel.formatting.bold(str(num + 1) + '.') + ' ' + definition + ' '
        if len(response.encode('utf-8') + formatted_definition.encode('utf-8')) < MESSAGE_LENGTH: # put whole response in single message, cut off earlier
            response += formatted_definition

    
    return response


@sopel.module.commands('kriu')
def kriu(bot, trigger):
    word = trigger.group(2)
    html = get_page(word)
    definitions = get_definitions(html)
    message = format_response(word, definitions)
    link = sopel.formatting.underline('nuoroda -> ' + DICT_URL + word)
    if len(message.encode('utf-8') + link.encode('utf-8')) < MESSAGE_LENGTH:
        bot.say(message + link)
    else:
        bot.say(message)
        bot.say(link)