import setuptools

setuptools.setup(
    name="kriu",
    author="Paulius",
    author_email="paulius@sukys.eu",
    description="Jaunimo žodyno botas",
    use_scm_version=True,
    packages=setuptools.find_packages(),
    install_requires=[
        "sopel",
        "requests",
        "beautifulsoup4"
    ],
    setup_requires=[
        "setuptools_scm"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        "sopel.plugins": ["kriu=kriu"]  
    },
    python_requires='>=3.6',
)
